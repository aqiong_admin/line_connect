# 简单的使用canvas的连线功能

#### 项目介绍
左边3个右边3个，使用canvas 实现的左右2边随便选一个建立连线

[在线体验](https://aqiong.top/line.html) （请切换到手机端体验）

仅支持手机端的演示，PC端未做适配
示列图片：
[点击查看](https://pan.baidu.com/s/1Oox2fbE0hLOYaRaupCnlgg)

![输入图片说明](https://aqiong.top/image/temp/line.jpg "在这里输入图片标题")
#### 安装教程

下载即可使用，如有帮助，点亮star.